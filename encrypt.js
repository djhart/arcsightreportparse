
"use strict";

const crypto = require('crypto');
const key = 'pronghorn';

const creds = {
    "user": "admin",
    "pass": "arcsight",
    "host": "sol-saturn",
    "port": "8443"
};
const plaintext = JSON.stringify(creds);

const cipher = crypto.createCipher('aes-256-cbc', key);

let encryptedPassword = cipher.update(plaintext, 'utf8', 'base64');
encryptedPassword += cipher.final('base64')

console.log('original  :', plaintext);
console.log('encrypted :', encryptedPassword);