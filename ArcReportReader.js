#!/bin/node
"use strict";

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

const _ = require('lodash');
const request = require('request');
const crypto = require('crypto');

const arcHash = process.env.arcHash;
const arcReport = process.env.arcReport;
const arcKey = process.env.arcKey;

const decipher = crypto.createDecipher('aes-256-cbc', arcKey);

let decryptedPassword = decipher.update(arcHash, 'base64', 'utf8');
decryptedPassword += decipher.final('utf8');

const conf = JSON.parse(decryptedPassword);

const urlPrefix = "https://" + conf.host + ":" + conf.port + "/www/";
const urlSuffix = "-service/rest";

const arcUser = encodeURIComponent(conf.user);
const arcPass = encodeURIComponent(conf.pass);

function init() {
    getToken(arcUser, arcPass, nowGetIds);
}

let start = function(authToken, myIds) {
    getReportIdFromName(authToken, myIds, processReport);
};

let nowGetIds = (data) => {
    getReportIds(data, start);
};

let processReport = (authToken, reportId) => {
    runReport(authToken, reportId, getAndPrint);
};

let getAndPrint = (id) => {
    getReportData(id);
};

function getReportIds(authToken, callback) {

    let options = {
        url: urlPrefix + "manager" + urlSuffix + '/ReportService/findAllIds?authToken=' + authToken + '&alt=json',
        json: true
    };

    request.get(options, (error, response, body) => {
        if (!error && response.statusCode === 200) {
            const idList = body["rep.findAllIdsResponse"]["rep.return"];
            callback(authToken, idList);
        }
    });
}

function getReportIdFromName(authToken, idList, callback) {

    idList.forEach((id) => {
        let options = {
            url: urlPrefix + "manager" + urlSuffix + '/ReportService/findByUUID?authToken=' + authToken + '&id=' + encodeURIComponent(id) + '&alt=json',
            json: true
        };

        request.get(options, function(error, response, body) {

            if (!error && response.statusCode === 200) {

                let myBody = body["rep.findByUUIDResponse"]["rep.return"];
                if (myBody.name === arcReport) {
                    callback(authToken, myBody.resourceid)
                }
            }
        });

    });
}

function getReportData(id) {
    let options = {
        //no urlSuffix below
        url: urlPrefix + "manager" + '-service/fileservlet?file.command=download&file.id=' + encodeURIComponent(id)
    };

    request.get(options, (error, response, body) => {
        if (!error && response.statusCode === 200) {
            console.log(body);
        }
    });
}

function runReport(authToken, reportId, callback) {

    let options = {
        url: urlPrefix + "manager" + urlSuffix + '/ArchiveReportService/initDefaultArchiveReportDownloadById?authToken=' + authToken + '&reportId=' + encodeURIComponent(reportId) + '&reportType=Manual',
        json: true
    };

    request.get(options, (error, response, body) => {

        if (!error && response.statusCode === 200) {
            const archiveId = body['arc.initDefaultArchiveReportDownloadByIdResponse']["arc.return"];
            callback(archiveId);
        }
    });
}

let getToken = (user, pass, callback) => {
    let options = {
        url: urlPrefix + "core" + urlSuffix + '/LoginService/login?login=' + user + '&password=' + pass,
        json: true
    };

    request.get(options, function(error, response, body) {
        if (!error && response.statusCode === 200) {
            let myBody = encodeURIComponent(body["log.loginResponse"]["log.return"]);
            callback(myBody);
        }
    });
};

init();
